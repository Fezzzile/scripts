## Why I wrote this script
After downloading music, I usually analyse each song in an album with Spek, an "Acoustic Spectrum Analyser." The problem with Spek is, it forces me to type the command "spek" after each analysis, as it does not take more than one command-line argument; so commands like "spek *flac" or "spek song1 song2" always return a warning message. I'm not sure if this is platform- or version-dependent (the current version as of this writing is 0.8.2.3). 

This script kind of fixes this problem. Simply typing "spekall" without any arguments will spek-analyse all (hopefully) the files with an audio stream, including videos. At first all I was concerned about were the .m4a (aac) or .flac files, which, when combined, make up more than 60% of my music collection. And yeah, I'm pretty much old-school—no streaming. Data is expensive in South Africa! 

## Dependencies
1. echo
2. egrep
3. file
4. spek
5. zsh

